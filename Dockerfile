FROM debian:bullseye
LABEL name="epubcheker"
LABEL version="0.1"

RUN \
  apt-get update && \
  apt-get install --no-install-recommends --yes default-jre epubcheck make && \
  apt-get clean && \
  rm -rf /var/lib/apt/lists/*

# Workaround for broken /usr/bin/epubcheck
RUN \
  printf '#! /bin/bash\n\njarwrapper /usr/bin/epubcheck "$@"\n' > /usr/local/bin/epubcheck && \
  chmod a+x /usr/local/bin/epubcheck

ENV PATH="${PATH}:/usr/local/bin/"
