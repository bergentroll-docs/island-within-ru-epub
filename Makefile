.PHONY: all test clean

all: src.epub

src.epub: src/**/*
	epubcheck --mode exp --save ./src/

test:
	epubcheck --mode exp ./src/

clean:
	rm -f src.epub
